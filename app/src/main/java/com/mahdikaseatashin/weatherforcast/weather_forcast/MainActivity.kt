package com.mahdikaseatashin.weatherforcast.weather_forcast

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.location.Location
import android.location.LocationManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.location.*
import com.google.android.material.snackbar.Snackbar
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.mahdikaseatashin.weatherforcast.R
import com.mahdikaseatashin.weatherforcast.adapter.WeatherRecyclerViewAdapter
import com.mahdikaseatashin.weatherforcast.models.ListModel
import com.mahdikaseatashin.weatherforcast.models.WeatherModel
import com.mahdikaseatashin.weatherforcast.network.WeatherService
import com.mahdikaseatashin.weatherforcast.utils.Constants
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity() {

    companion object {
        const val TAG = "MainActivity"
    }

    private lateinit var mFusedLocationProviderClient: FusedLocationProviderClient
    private lateinit var weatherList: WeatherModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        checkPhoneLocationAccess()
    }

    private fun checkPhoneLocationAccess() {
        if (!isLocationEnabled()) {
            val snackBar = Snackbar.make(
                parent_layout_main,
                "Turn on location provider!",
                Snackbar.LENGTH_LONG
            ).setAction(
                "Setting"
            ) {
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)
            }
            snackBar.show()
        } else {
            Dexter.withContext(this).withPermissions(
                android.Manifest.permission.ACCESS_FINE_LOCATION,
                android.Manifest.permission.ACCESS_COARSE_LOCATION
            ).withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                    if (report!!.areAllPermissionsGranted()) {

                        requestLocationData()

                    } else if (report.isAnyPermissionPermanentlyDenied) {
                        val snackBar = Snackbar.make(
                            parent_layout_main,
                            "Give location permission",
                            Snackbar.LENGTH_LONG
                        ).setAction(
                            "Setting"
                        ) {
                            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                            val uri: Uri = Uri.fromParts("package", packageName, null)
                            intent.data = uri
                            startActivity(intent)
                        }
                        snackBar.show()
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    p0: MutableList<PermissionRequest>?,
                    token: PermissionToken?
                ) {
                    showLocationPermissionDialog()
                }

            }).onSameThread().check()
        }
    }

    private fun isLocationEnabled(): Boolean {
        val locationManager: LocationManager =
            getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
                || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
    }

    private fun showLocationPermissionDialog() {
        AlertDialog.Builder(this@MainActivity)
            .setTitle("Permission")
            .setMessage("Please give us the permission to access the location")
            .setPositiveButton("Setting") { _, _ ->
                val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                val uri = Uri.fromParts("package", packageName, null)
                intent.data = uri
                startActivity(intent)
            }.setNegativeButton("Cancel") { dialog, _ ->
                dialog.dismiss()
            }
            .show()
    }

    @SuppressLint("MissingPermission")
    private fun requestLocationData() {
        val mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mFusedLocationProviderClient.requestLocationUpdates(
            mLocationRequest,
            mLocationCallback,
            Looper.myLooper()!!
        )
    }

    private val mLocationCallback = object : LocationCallback() {
        override fun onLocationResult(result: LocationResult) {
            val mLastLocation: Location = result.lastLocation
            val lat = mLastLocation.latitude
            val lng = mLastLocation.longitude
            Log.e(TAG, "onLocationResult:\n lat : $lat\n lng : $lng")
            getLocationWeatherDetails(lat, lng)
        }
    }

    private fun getLocationWeatherDetails(lat: Double, lng: Double) {
        if (Constants.isNetworkAvailable(this)) {
            val retrofit: Retrofit = Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()

            val service: WeatherService =
                retrofit.create(WeatherService::class.java)
            val listCall: Call<WeatherModel> =
                service.getWeather(
                    lat, lng, Constants.APP_ID, Constants.METRIC_UNIT
                )
            listCall.enqueue(object : Callback<WeatherModel> {
                override fun onResponse(
                    call: Call<WeatherModel>,
                    response: Response<WeatherModel>
                ) {
                    if (response.isSuccessful) {
                        weatherList = response.body()!!
                        initializeViews()
//                        Log.e(TAG, "onResponse: \n $weatherList")
                    } else {
                        Log.e(TAG, "onResponse: ${response.code()}")
                    }
                }

                override fun onFailure(call: Call<WeatherModel>, t: Throwable) {
                    Toast.makeText(
                        this@MainActivity,
                        "something went wrong in get request",
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.e(TAG, "onFailure: ${t.message.toString()}")
                }

            })
        } else {
            Toast.makeText(this, "Not Connected", Toast.LENGTH_SHORT).show()
        }
    }

    private fun initializeViews() {
        androidx.transition.TransitionManager.beginDelayedTransition(parent_layout_main)
        circularProgressIndicator.visibility = View.GONE
        tv_wait.visibility = View.GONE
        val calender = Calendar.getInstance()
        val dateFormat =  SimpleDateFormat("yyyy, MMMM, dd hh:mm aaa",Locale.getDefault())
        val date = dateFormat.format(calender.time)
        tv_date.text = date.toString()
        tv_metrics.visibility = View.VISIBLE
        tv_metrics2.visibility = View.VISIBLE
        tv_temp.text = weatherList.list[0].main.temp.toString()
        tv_city.text = weatherList.city.name
        val list : ArrayList<ListModel> = weatherList.list
        var rv = findViewById<RecyclerView>(R.id.rv_main)
        rv.layoutManager = LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false)
        val weatherAdapter = WeatherRecyclerViewAdapter(list,this)
        rv.adapter = weatherAdapter
        weatherAdapter.setOnClickListener(object : WeatherRecyclerViewAdapter.OnClickListener {
            override fun onClick(position: Int, model: ListModel) {
                Log.e(TAG, "WTFFFFFFFFFFFF" )
                setToCurrent(model)
            }
        })

    }

    fun setToCurrent(model: ListModel) {
        val date = Date(model.dt * 1000L)
        val sdf =  SimpleDateFormat("yyyy, MMMM, dd hh:mm aaa",Locale.getDefault())
        sdf.timeZone = TimeZone.getDefault()
        tv_date.text = sdf.format(date)

        tv_temp.text = model.main.temp.toString()

    }
}
